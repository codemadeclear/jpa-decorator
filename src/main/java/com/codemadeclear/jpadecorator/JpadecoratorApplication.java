package com.codemadeclear.jpadecorator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpadecoratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpadecoratorApplication.class, args);
	}

}
