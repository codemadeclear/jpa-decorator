package com.codemadeclear.jpadecorator.model.entities;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Accessors(chain = true)
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private Integer publishingYear;

    @OneToMany(mappedBy = "book", cascade = CascadeType.PERSIST)
    @ToString.Exclude
    private List<BookAuthor> bookAuthors = new ArrayList<>();

    public Book addAuthor(Author author) {
        this.bookAuthors.add(new BookAuthor().setBook(this).setAuthor(author));
        return this;
    }

    @OneToMany(mappedBy = "book", cascade = CascadeType.PERSIST)
    @ToString.Exclude
    private List<Review> reviews = new ArrayList<>();

    public Book addReview(Review review) {
        this.reviews.add(review);
        review.setBook(this);
        return this;
    }

    @OneToMany(mappedBy = "book", cascade = CascadeType.PERSIST)
    @ToString.Exclude
    private List<BookAward> bookAwards = new ArrayList<>();

    public Book addAward(Award award) {
        this.bookAwards.add(new BookAward().setBook(this).setAward(award));
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;

        Book book = (Book) o;

        return this.getId() != null && this.getId().equals(book.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }

}
