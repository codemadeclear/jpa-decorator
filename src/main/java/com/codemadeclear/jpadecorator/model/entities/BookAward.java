package com.codemadeclear.jpadecorator.model.entities;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Data
@Accessors(chain = true)
public class BookAward {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BOOK_ID", nullable = false)
    @ToString.Exclude
    private Book book;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AWARD_ID", nullable = false)
    @ToString.Exclude
    private Award award;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BookAward)) return false;

        BookAward bookAward = (BookAward) o;

        return this.getId() != null && this.getId().equals(bookAward.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }

}
