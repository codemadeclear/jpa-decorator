package com.codemadeclear.jpadecorator.model.entities;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Accessors(chain = true)
public class Award {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "award")
    @ToString.Exclude
    private List<BookAward> bookAwards = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Award)) return false;

        Award award = (Award) o;

        return this.getId() != null && this.getId().equals(award.getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }

}
