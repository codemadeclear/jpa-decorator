package com.codemadeclear.jpadecorator.services;

import javax.persistence.EntityManager;

/**
 * Interface implemented by classes that expose a getter for the {@link EntityManager}
 */
public interface PersistenceContextAware {

    EntityManager getEntityManager();

}
