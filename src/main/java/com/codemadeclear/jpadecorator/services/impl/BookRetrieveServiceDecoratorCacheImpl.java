package com.codemadeclear.jpadecorator.services.impl;

import com.codemadeclear.jpadecorator.services.AbstractBookAssociationRetrieveDecorator;
import com.codemadeclear.jpadecorator.services.BookRetrieveService;
import com.codemadeclear.jpadecorator.services.BookRetrieveServiceDecoratorCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class BookRetrieveServiceDecoratorCacheImpl implements BookRetrieveServiceDecoratorCache {

    @Autowired
    private ApplicationContext applicationContext;
    private final Object mutex = new Object();

    @Override
    public BookRetrieveService getOrRegister(BookRetrieveService service) {
        Optional<BookRetrieveService> serviceOptional = getFromContext(service);
        if (serviceOptional.isPresent()) {
            return serviceOptional.get();
        } else { //the following block is synchronized to prevent duplicated registrations of equivalent decorator beans
            synchronized (mutex) {
                if (isNotPresent(service)) {
                    registerBookRetrieveServiceBean(service);
                }
            }
            return getOrRegister(service);
        }
    }

    private Optional<BookRetrieveService> getFromContext(BookRetrieveService service) {
        return getStreamOfBookRetrieveServiceBeans()
                .filter(beanHasSameDecoratorsChainSetWith(service))
                .findFirst();
    }

    private Stream<BookRetrieveService> getStreamOfBookRetrieveServiceBeans() {
        return applicationContext.getBeansOfType(BookRetrieveService.class).values().stream();
    }

    private Predicate<BookRetrieveService> beanHasSameDecoratorsChainSetWith(BookRetrieveService service) {
        return bean -> getDecoratorsChainSet(bean).equals(getDecoratorsChainSet(service));
    }

    private Set<Class<? extends BookRetrieveService>> getDecoratorsChainSet(BookRetrieveService service) {
        return (Set<Class<? extends BookRetrieveService>>) service.getDecoratorsChain(HashSet::new);
    }

    private boolean isNotPresent(BookRetrieveService service) {
        return getStreamOfBookRetrieveServiceBeans()
                .noneMatch(beanHasSameDecoratorsChainSetWith(service));
    }

    private void registerBookRetrieveServiceBean(BookRetrieveService service) {
        GenericApplicationContext ac = (GenericApplicationContext) applicationContext;
        ac.registerBean(
                getBeanName(service),
                service.getClass(),
                //the argument BookRetrieveService provided to the decorator's constructor must be a managed bean
                //as well, if the method argument 'service' is the bean bookLazilyRetrieveService we can't get in
                //this execution branch because the class is a @Service bean, hence the cast is safe
                getOrRegister(((AbstractBookAssociationRetrieveDecorator) service).getBookRetrieveService())
        );
    }

    private String getBeanName(BookRetrieveService service) {
        String chainListAsString = getChainListAsString(service, "_");
        return lowercaseFirstChar(chainListAsString);
    }

    @SuppressWarnings("SameParameterValue")
    private String getChainListAsString(BookRetrieveService service, String separator) {
        return service.getDecoratorsChain(LinkedList::new).stream()
                .map(Class::getSimpleName)
                .collect(Collectors.joining(separator));
    }

    private String lowercaseFirstChar(String s) {
        return s.substring(0, 1).toLowerCase().concat(s.substring(1));
    }

}
