package com.codemadeclear.jpadecorator.services.impl;

import com.codemadeclear.jpadecorator.model.entities.Book;
import com.codemadeclear.jpadecorator.services.AbstractBookAssociationRetrieveDecorator;
import com.codemadeclear.jpadecorator.services.BookLazilyRetrieveService;
import com.codemadeclear.jpadecorator.services.BookRetrieveService;
import com.codemadeclear.jpadecorator.services.BookRetrieveServiceDecoratorCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BookLazilyRetrieveServiceImpl implements BookLazilyRetrieveService {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private BookRetrieveServiceDecoratorCache decoratorCache;

    @Override
    @Transactional
    public Optional<Book> byId(Long id) {
        return Optional.ofNullable(em.find(Book.class, id));
    }

    @Override
    @Transactional
    public List<Book> byAuthorId(Long authorId) {
        return em.createQuery(
                " SELECT DISTINCT b FROM Book b " +
                        " JOIN b.bookAuthors ba " +
                        " WHERE ba.author.id = :authorId", Book.class)
                .setParameter("authorId", authorId)
                .getResultList();
    }

    @Override
    @Transactional
    public List<Book> byPublishingYear(Integer publishingYear) {
        return em.createQuery(
                " SELECT b FROM Book b " +
                        " WHERE b.publishingYear = :year ", Book.class)
                .setParameter("year", publishingYear)
                .getResultList();
    }

    @Override
    public EntityManager getEntityManager() {
        return this.em;
    }

    @Override
    public BookLazilyRetrieveService.Builder decoratedBy(
            Class<? extends AbstractBookAssociationRetrieveDecorator> decoratorClass) {
        return new Builder(this, decoratorClass, decoratorCache);
    }

    public static class Builder implements BookLazilyRetrieveService.Builder {

        public static final String ABSTRACT_DECORATOR_CLASS_ERR_MSG =
                "The BookRetrieveService must be decorated by a concrete decorator, an abstract class was provided: ";
        private final BookRetrieveService service;
        //we want to prevent a clumsy programmer to mistakenly apply the same decorator twice or more since it would imply
        //unnecessary repetition of the same query, we also need to guarantee consistent decorators' sorting, in order to
        //maximize re-use of already existing decorator beans from the application context, hence the TreeSet
        private final Set<Class<? extends AbstractBookAssociationRetrieveDecorator>> decoratorClasses =
                new TreeSet<>(Comparator.comparing(Class::getSimpleName));
        private final BookRetrieveServiceDecoratorCache decoratorCache;

        private Builder(BookRetrieveService service,
                        Class<? extends AbstractBookAssociationRetrieveDecorator> decoratorClass,
                        BookRetrieveServiceDecoratorCache decoratorCache) {
            this.service = service;
            addDecoratorClass(decoratorClass);
            this.decoratorCache = decoratorCache;
        }

        @Override
        public BookLazilyRetrieveService.Builder decoratedBy(
                Class<? extends AbstractBookAssociationRetrieveDecorator> decoratorClass) {
            addDecoratorClass(decoratorClass);
            return this;
        }

        private void addDecoratorClass(Class<? extends AbstractBookAssociationRetrieveDecorator> decoratorClass) {
            assertClassIsNotAbstract(decoratorClass);
            this.decoratorClasses.add(decoratorClass);
        }

        private void assertClassIsNotAbstract(
                Class<? extends AbstractBookAssociationRetrieveDecorator> decoratorClass) {
            if (Modifier.isAbstract(decoratorClass.getModifiers()))
                throw new IllegalArgumentException(ABSTRACT_DECORATOR_CLASS_ERR_MSG + decoratorClass.getSimpleName());
        }

        @Override
        public BookRetrieveService get() {
            Set<AbstractBookAssociationRetrieveDecorator> decoratorsToApply =
                    instantiateAll(this.decoratorClasses);
            AbstractBookAssociationRetrieveDecorator decoratedService =
                    applyDecorators(
                            this.service,
                            decoratorsToApply
                    );
            return decoratorCache.getOrRegister(decoratedService);
        }

        private Set<AbstractBookAssociationRetrieveDecorator> instantiateAll(
                    Set<Class<? extends AbstractBookAssociationRetrieveDecorator>> decoratorClassesToApply) {
            return decoratorClassesToApply.stream()
                    .map(this::instantiate)
                    .collect(Collectors.toCollection(LinkedHashSet::new)); //LinkedHashSet is necessary to preserve sorting
        }

        private AbstractBookAssociationRetrieveDecorator instantiate(
                                    Class<? extends AbstractBookAssociationRetrieveDecorator> decoratorClass) {
            try {
                Constructor<? extends AbstractBookAssociationRetrieveDecorator> noArgsConstructor =
                        decoratorClass.getConstructor();
                return noArgsConstructor.newInstance();
            } catch (ReflectiveOperationException e) {
                throw new RuntimeException("ReflectiveOperationException occurred", e);
            }
        }

        private AbstractBookAssociationRetrieveDecorator applyDecorators(
                                                  BookRetrieveService toDecorate,
                                                  Set<AbstractBookAssociationRetrieveDecorator> decorators) {
            if (decorators.isEmpty()) {
                return (AbstractBookAssociationRetrieveDecorator) toDecorate;
            } else {
                Iterator<AbstractBookAssociationRetrieveDecorator> iterator = decorators.iterator();
                AbstractBookAssociationRetrieveDecorator decorator = iterator.next();
                iterator.remove();
                return applyDecorators(
                        toDecorate.accept(decorator),
                        decorators
                );
            }
        }

    }

}
