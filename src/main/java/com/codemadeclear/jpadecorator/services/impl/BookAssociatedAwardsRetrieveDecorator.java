package com.codemadeclear.jpadecorator.services.impl;

import com.codemadeclear.jpadecorator.model.entities.Book;
import com.codemadeclear.jpadecorator.services.AbstractBookAssociationRetrieveDecorator;
import com.codemadeclear.jpadecorator.services.BookRetrieveService;
import org.hibernate.annotations.QueryHints;

import java.util.List;

public class BookAssociatedAwardsRetrieveDecorator extends AbstractBookAssociationRetrieveDecorator {

    @SuppressWarnings("unused")
    public  BookAssociatedAwardsRetrieveDecorator() {
        super();
    }

    private BookAssociatedAwardsRetrieveDecorator(BookRetrieveService bookRetrieveService) {
        super(bookRetrieveService);
    }

    @Override
    public AbstractBookAssociationRetrieveDecorator decorate(BookRetrieveService bookRetrieveService) {
        return new BookAssociatedAwardsRetrieveDecorator(bookRetrieveService);
    }

    @Override
    protected Book fetchWithAssociation(Book book) {
        return getEntityManager().createQuery(
                " SELECT DISTINCT b " +
                        " FROM Book b " +
                        " LEFT JOIN FETCH b.bookAwards ba " +
                        " LEFT JOIN FETCH ba.award a " +
                        " WHERE b = :book", Book.class)
                .setParameter("book", book)
                .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .getSingleResult();
    }

    @Override
    protected List<Book> fetchWithAssociation(List<Book> books) {
        return getEntityManager().createQuery(
                " SELECT DISTINCT b " +
                        " FROM Book b " +
                        " LEFT JOIN FETCH b.bookAwards ba " +
                        " LEFT JOIN FETCH ba.award a " +
                        " WHERE b IN :books", Book.class)
                .setParameter("books", books)
                .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
                .getResultList();
    }

}
