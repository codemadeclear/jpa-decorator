package com.codemadeclear.jpadecorator.services;

import org.springframework.context.ApplicationContext;

/**
 * Interface extending {@link BookRetrieveService}, this interface signals that all the implemented
 * methods inherited from {@link BookRetrieveService} do not initialize entity associations.
 */
public interface BookLazilyRetrieveService extends BookRetrieveService {

    /**
     * Single point of entrance for the {@link Builder}'s API
     *
     * @return an instance of {@link Builder} referencing the instance of {@link BookRetrieveService} upon which this
     * method was called. The original instance of {@link BookRetrieveService} will not be modified, it will be the basis
     * for the instantiation and retrieval/registration of the decorated service
     */
    Builder decoratedBy(Class<? extends AbstractBookAssociationRetrieveDecorator> decoratorClass);

    /**
     * This sub-interface is meant to encapsulate the decoration of the {@link BookLazilyRetrieveService}'s
     * implementation, in order to allow client code to instantiate a decorated service in a fluent, declarative way
     */
    interface Builder {

        /**
         * Adds the given subclass of {@link AbstractBookAssociationRetrieveDecorator} to this builders'
         * chain of decorators. Implementations should prevent multiple addition of the same class to the decorators'
         * chain
         *
         * @return the <code>Builder</code>'s instance with its inner status updated
         * @throws IllegalArgumentException if the argument class is an abstract class
         */
        Builder decoratedBy(Class<? extends AbstractBookAssociationRetrieveDecorator> decoratorClass);

        /**
         * Retrieves from the {@link ApplicationContext} a managed {@link BookRetrieveService} bean having the same
         * set of decorators as the ones in this Builder's inner status and returns it.
         * If no such bean is found in the {@link ApplicationContext} it will be registered and then returned
         * @see BookRetrieveServiceDecoratorCache#getOrRegister(BookRetrieveService)
         */
        BookRetrieveService get();

    }

}
