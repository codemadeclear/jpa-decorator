package com.codemadeclear.jpadecorator.services;

import com.codemadeclear.jpadecorator.model.entities.Book;

import java.util.*;
import java.util.function.Supplier;

/**
 * Interface defining methods for {@link Book} entity retrieval. This interface is not meant to be injected by type,
 * rather inject {@link BookLazilyRetrieveService} or {@link AbstractBookAssociationRetrieveDecorator}
 */
public interface BookRetrieveService extends PersistenceContextAware, Visitable<AbstractBookAssociationRetrieveDecorator> {

    Optional<Book> byId(Long id);

    List<Book> byAuthorId(Long authorId);

    List<Book> byPublishingYear(Integer publishingYear);

    default BookRetrieveService accept(AbstractBookAssociationRetrieveDecorator visitor) {
        return visitor.decorate(this);
    }

    default Collection<Class<? extends BookRetrieveService>> getDecoratorsChain(
                Supplier<Collection<Class<? extends BookRetrieveService>>> supplier) {
        Collection<Class<? extends BookRetrieveService>> chain = supplier.get();
        chain.add(this.getClass());
        return chain;
    }

}
