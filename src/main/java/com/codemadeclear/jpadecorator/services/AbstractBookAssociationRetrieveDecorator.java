package com.codemadeclear.jpadecorator.services;

import com.codemadeclear.jpadecorator.model.entities.Book;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;


/**
 * Basis abstract class to be extended by all concrete decorator classes that fetch entities associated to the
 * {@link Book} entity
 */
@SuppressWarnings("FieldMayBeFinal")
public abstract class AbstractBookAssociationRetrieveDecorator implements BookRetrieveService {

    private static final String TO_STRING_DELIMITER = " decorated by ";
    private BookRetrieveService bookRetrieveService;

    /**
     * The no-args constructor is only meant to be used by an implementation of {@link BookLazilyRetrieveService.Builder}
     * to instantiate a concrete decorator from the corresponding {@link Class} object. All concrete classes extending
     * this abstract class should provide such a no-args constructor
     */
    public AbstractBookAssociationRetrieveDecorator() {
        this.bookRetrieveService = null;
    }

    public AbstractBookAssociationRetrieveDecorator(BookRetrieveService bookRetrieveService) {
        this.bookRetrieveService = bookRetrieveService;
    }

    public abstract AbstractBookAssociationRetrieveDecorator decorate(BookRetrieveService bookRetrieveService);

    @Override
    @Transactional
    public Optional<Book> byId(Long id) {
        Optional<Book> bookOptional = bookRetrieveService.byId(id);
        return retrieveAssociation(bookOptional.orElse(null));
    }

    private Optional<Book> retrieveAssociation(Book bookFromService) {
        if (bookFromService != null) {
            Book ownRetrievedBook = fetchWithAssociation(bookFromService);
            return Optional.of(ownRetrievedBook);
        } else { //if the inner decorated service returned an empty optional we're not wasting time with a useless query
            return Optional.empty();
        }
    }

    protected abstract Book fetchWithAssociation(Book book);

    @Override
    @Transactional
    public List<Book> byAuthorId(Long authorId) {
        List<Book> booksFromService = bookRetrieveService.byAuthorId(authorId);
        return retrieveAssociation(booksFromService);
    }

    private List<Book> retrieveAssociation(List<Book> booksFromService) {
        if (!booksFromService.isEmpty()) {
            return fetchWithAssociation(booksFromService);
        } else { //if the inner decorated service returned an empty list we're not wasting time with a useless query
            return Collections.emptyList();
        }
    }

    protected abstract List<Book> fetchWithAssociation(List<Book> books);

    @Override
    @Transactional
    public List<Book> byPublishingYear(Integer publishingYear) {
        List<Book> booksFromService = bookRetrieveService.byPublishingYear(publishingYear);
        return retrieveAssociation(booksFromService);
    }

    @Override
    public EntityManager getEntityManager() {
        return this.bookRetrieveService.getEntityManager();
    }

    public BookRetrieveService getBookRetrieveService() {
        return this.bookRetrieveService;
    }

    @Override
    public Collection<Class<? extends BookRetrieveService>> getDecoratorsChain(
            Supplier<Collection<Class<? extends BookRetrieveService>>> supplier) {
        Collection<Class<? extends BookRetrieveService>> chain = this.bookRetrieveService.getDecoratorsChain(supplier);
        chain.add(this.getClass());
        return chain;
    }

    @Override
    public String toString() {
        return getDecoratorsChain(LinkedList::new).stream()
                .map(Class::getSimpleName)
                .collect(Collectors.joining(TO_STRING_DELIMITER));
    }

}
