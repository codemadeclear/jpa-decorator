package com.codemadeclear.jpadecorator.services;

public interface Visitable<V> {

    Visitable<V> accept(V visitor);

}
