package com.codemadeclear.jpadecorator.services;

import org.springframework.context.ApplicationContext;

/**
 * Interface implemented by classes capable of retrieving managed beans from the {@link ApplicationContext} that
 * implement {@link BookRetrieveService} with any set of decorators applied. The implementation could also register
 * such beans if necessary
 */
public interface BookRetrieveServiceDecoratorCache {

    /**
     * Retrieves from the {@link ApplicationContext} a {@link BookRetrieveService} bean matching the same decorators
     * chain set as the {@link BookRetrieveService} entity provided as parameter. If no such managed bean exists it will
     * be registered and then returned.
     * The parameter {@link BookRetrieveService} entity need not be a managed bean, but the returned entity will.
     * All instances of {@link BookRetrieveService} in the returned bean's decorators chain will be managed beans as well.
     * Implementations should synchronize the bean registration, in order to prevent two different threads from
     * registering two different but equivalent {@link BookRetrieveService} beans having the same decorators chain set
     */
    BookRetrieveService getOrRegister(BookRetrieveService service);

}
