package com.codemadeclear.jpadecorator.services;

import com.codemadeclear.jpadecorator.MySpringBootTest;
import com.codemadeclear.jpadecorator.services.impl.BookAssociatedAuthorsRetrieveDecorator;
import com.codemadeclear.jpadecorator.services.impl.BookAssociatedAwardsRetrieveDecorator;
import com.codemadeclear.jpadecorator.services.impl.BookAssociatedReviewsRetrieveDecorator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.util.LinkedList;
import java.util.Map;

import static com.codemadeclear.jpadecorator.services.impl.BookLazilyRetrieveServiceImpl.Builder.ABSTRACT_DECORATOR_CLASS_ERR_MSG;
import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("WeakerAccess")
public class BookLazilyRetrieveServiceBuilderTest extends MySpringBootTest {

    @Autowired
    private BookLazilyRetrieveService bookLazilyRetrieveService;
    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void givenNewApplicationContext_whenDecoratingServiceOnce_thenTwoRetrieveServiceFoundInApplicationContext() {
        bookLazilyRetrieveService.decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class).get();
        Map<String, BookRetrieveService> beans = getBookRetrieveServiceBeansFromApplicationContext();
        assertEquals(2, beans.size());
    }

    private Map<String, BookRetrieveService> getBookRetrieveServiceBeansFromApplicationContext() {
        return applicationContext.getBeansOfType(BookRetrieveService.class);
    }


    @Test
    public void givenNewApplicationContext_whenDecoratingServiceTwice_thenThreeRetrieveServiceFoundInApplicationContext() {
        bookLazilyRetrieveService.decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class).get();
        bookLazilyRetrieveService.decoratedBy(BookAssociatedReviewsRetrieveDecorator.class).get();
        Map<String, BookRetrieveService> beans = getBookRetrieveServiceBeansFromApplicationContext();
        assertEquals(3, beans.size());
    }

    @Test
    public void givenNewApplicationContext_whenDecorationIsRepeated_thenNoBeanAddedToApplicationContext() {
        bookLazilyRetrieveService.decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class).get();
        BookRetrieveService decoratedBean2 = bookLazilyRetrieveService.decoratedBy(BookAssociatedReviewsRetrieveDecorator.class).get();
        BookRetrieveService decoratedBean3 = bookLazilyRetrieveService.decoratedBy(BookAssociatedReviewsRetrieveDecorator.class).get();
        Map<String, BookRetrieveService> beans = getBookRetrieveServiceBeansFromApplicationContext();
        assertEquals(3, beans.size());
        assertEquals(decoratedBean2, decoratedBean3);
    }

    @Test
    public void givenNewApplicationContext_whenDecoratingSameServiceTwice_thenThreeRetrieveServiceFoundInApplicationContext() {
        bookLazilyRetrieveService
                .decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class)
                .decoratedBy(BookAssociatedReviewsRetrieveDecorator.class)
                .get();
        Map<String, BookRetrieveService> beans = getBookRetrieveServiceBeansFromApplicationContext();
        assertEquals(3, beans.size());
    }

    @Test
    public void givenNewApplicationContext_whenApplyingSameDecoratorsInDifferentOrder_thenSameDecoratedServiceIsRetrieved() {
        BookRetrieveService decoratedBean1 = bookLazilyRetrieveService
                .decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class)
                .decoratedBy(BookAssociatedReviewsRetrieveDecorator.class)
                .get();
        BookRetrieveService decoratedBean2 = bookLazilyRetrieveService
                .decoratedBy(BookAssociatedReviewsRetrieveDecorator.class)
                .decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class)
                .get();
        Map<String, BookRetrieveService> beans = getBookRetrieveServiceBeansFromApplicationContext();
        assertEquals(3, beans.size());
        assertEquals(decoratedBean1, decoratedBean2);
    }

    /*
     * The purpose of this test is to guarantee that the minimum necessary amount of beans will be added to the
     * application context, i.e. any combination of N decorators already in the application context will be used as basis
     * for the creation of a combination of N+1 decorators that adds one more decorator to the set
     * */
    @Test
    public void givenNewApplicationContext_whenDecoratingTwiceWithOneExtraDecorator_thenOnlyOneExtraBeanRegisteredRegardlessOfDecoratorsOrder() {
        bookLazilyRetrieveService
                .decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class)
                .decoratedBy(BookAssociatedAwardsRetrieveDecorator.class)
                .get(); // decorators applied in order A, B
        Map<String, BookRetrieveService> beans = getBookRetrieveServiceBeansFromApplicationContext();
        assertEquals(3, beans.size());
        bookLazilyRetrieveService
                .decoratedBy(BookAssociatedReviewsRetrieveDecorator.class)
                .decoratedBy(BookAssociatedAwardsRetrieveDecorator.class)
                .decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class)
                .get(); // decorators applied in order C, B, A
        beans = getBookRetrieveServiceBeansFromApplicationContext();
        assertEquals(4, beans.size());
    }

    /*
     * This test is very important as it guarantees that the same decorator cannot be applied more than once to a given
     * BookRetrieveService, otherwise the same query would be redundantly run several times, thus worsening performance
     * */
    @Test
    public void givenNewApplicationContext_whenTryingToApplySameDecoratorSeveralTimes_thenDecoratorAppliedOnlyOnce() {
        BookRetrieveService decoratedBean = bookLazilyRetrieveService
                .decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class)
                .decoratedBy(BookAssociatedAwardsRetrieveDecorator.class)
                .decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class)
                .decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class)
                .get(); // decorators applied in order A, B, A, A
        Map<String, BookRetrieveService> beans = getBookRetrieveServiceBeansFromApplicationContext();
        assertEquals(3, beans.size());
        assertEquals(3, decoratedBean.getDecoratorsChain(LinkedList::new).size());
    }

    @Test
    public void givenNewApplicationContext_whenTryingToApplyAbstractDecorator_thenExceptionThrown() {
        try {
            bookLazilyRetrieveService
                    .decoratedBy(AbstractBookAssociationRetrieveDecorator.class)
                    .get();
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
            assertTrue(e.getMessage().startsWith(ABSTRACT_DECORATOR_CLASS_ERR_MSG));
            return;
        }
        fail();
    }

    @Test
    public void givenNewApplicationContext_whenTryingToApplyAbstractDecoratorAfterConcreteDecorator_thenExceptionThrown() {
        try {
            bookLazilyRetrieveService
                    .decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class)
                    .decoratedBy(AbstractBookAssociationRetrieveDecorator.class)
                    .get();
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
            assertTrue(e.getMessage().startsWith(ABSTRACT_DECORATOR_CLASS_ERR_MSG));
            return;
        }
        fail();
    }

}
