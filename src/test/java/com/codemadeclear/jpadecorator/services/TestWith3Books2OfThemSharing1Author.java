package com.codemadeclear.jpadecorator.services;

import com.codemadeclear.jpadecorator.model.entities.Author;
import com.codemadeclear.jpadecorator.model.entities.Book;
import com.codemadeclear.jpadecorator.model.entities.Review;
import com.codemadeclear.jpadecorator.repositories.AuthorRepository;
import com.codemadeclear.jpadecorator.repositories.BookRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class TestWith3Books2OfThemSharing1Author extends TestWith1Book2Authors2Awards3Reviews {

    protected static final String AUTHOR_3_NAME = "Third Author";
    protected static final Author AUTHOR_3 = new Author().setName(AUTHOR_3_NAME);
    protected static final int REVIEW_4_RATING = 4;
    protected static final String REVIEW_4_TITLE = "COOL";
    protected static final String REVIEW_4_CONTENT = "I really enjoyed this book";
    protected static final Review REVIEW_4 = new Review().setRating(REVIEW_4_RATING).setTitle(REVIEW_4_TITLE).setContent(REVIEW_4_CONTENT);
    protected static final int REVIEW_5_RATING = 1;
    protected static final String REVIEW_5_TITLE = "DISGUSTING";
    protected static final String REVIEW_5_CONTENT = "I hate this book!!!";
    protected static final Review REVIEW_5 = new Review().setRating(REVIEW_5_RATING).setTitle(REVIEW_5_TITLE).setContent(REVIEW_5_CONTENT);
    protected static final String BOOK_2_TITLE = "John Doe's 2nd BOOK";
    protected static final int BOOK_2_PUBLISHING_YEAR = 2020;
    protected static final Book BOOK_2 = new Book().setTitle(BOOK_2_TITLE).setPublishingYear(BOOK_2_PUBLISHING_YEAR)
            .addAuthor(AUTHOR_1).addAuthor(AUTHOR_3)
            .addReview(REVIEW_4).addReview(REVIEW_5);
    protected static final String BOOK_3_TITLE = "My first solo book";
    protected static final Integer BOOK_3_PUBLISHING_YEAR = 2020;
    protected static final Book BOOK_3 = new Book().setTitle(BOOK_3_TITLE).setPublishingYear(BOOK_3_PUBLISHING_YEAR)
            .addAuthor(AUTHOR_3);
    protected static final List<Book> ADDITIONAL_BOOKS = Arrays.asList(BOOK_2, BOOK_3);
    protected static final List<Review> BOOK_2_REVIEWS = Arrays.asList(REVIEW_4, REVIEW_5);

    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private AuthorRepository authorRepository;

    @BeforeEach
    public void setUp() {
        super.setUp();
        authorRepository.save(AUTHOR_3);
        bookRepository.saveAll(ADDITIONAL_BOOKS);
    }

    @AfterEach
    public void tearDown() {
        super.tearDown();
        AUTHOR_3.setId(null);
        ADDITIONAL_BOOKS.forEach(book -> book.setId(null));
        BOOK_2_REVIEWS.forEach(review -> review.setId(null));
        ADDITIONAL_BOOKS.stream().flatMap(book -> book.getBookAuthors().stream()).forEach(bookAuthor -> bookAuthor.setId(null));
    }

}
