package com.codemadeclear.jpadecorator.services;

import com.codemadeclear.jpadecorator.model.entities.Book;
import com.codemadeclear.jpadecorator.model.entities.BookAuthor;
import com.codemadeclear.jpadecorator.model.entities.Review;
import com.codemadeclear.jpadecorator.services.impl.BookAssociatedAuthorsRetrieveDecorator;
import com.codemadeclear.jpadecorator.services.impl.BookAssociatedReviewsRetrieveDecorator;
import org.hibernate.Hibernate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("WeakerAccess")
public class BookLazilyRetrieveServiceTest extends TestWith3Books2OfThemSharing1Author {

    @Autowired
    private BookLazilyRetrieveService bookLazilyRetrieveService;

    @Test
    public void givenNoDecoratorsApplied_whenFetchingById_thenNoAssociationIsInitialized() {
        BookRetrieveService bookRetrieveService = bookLazilyRetrieveService;
        Optional<Book> bookOptional = bookRetrieveService.byId(BOOK_1.getId());
        Book book = (Book) assertOptionalNotEmpty(bookOptional);
        assertBookAuthorsNotInitialized(book);
        assertBookAwardsNotInitialized(book);
        assertReviewsNotInitialized(book);
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private Object assertOptionalNotEmpty(Optional<?> optional) {
        assertTrue(optional.isPresent());
        return optional.get();
    }

    private void assertBookAuthorsNotInitialized(Book book) {
        assertFalse(Hibernate.isInitialized(book.getBookAuthors()));
    }

    private void assertBookAwardsNotInitialized(Book book) {
        assertFalse(Hibernate.isInitialized(book.getBookAwards()));
    }

    private void assertReviewsNotInitialized(Book book) {
        assertFalse(Hibernate.isInitialized(book.getReviews()));
    }

    @Test
    public void givenAuthorDecoratorApplied_whenFetchingById_thenAuthorsAssociationIsInitialized() {
        BookRetrieveService bookRetrieveService = bookLazilyRetrieveService.decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class)
                .get();
        Optional<Book> bookOptional = bookRetrieveService.byId(BOOK_1.getId());
        Book book = (Book) assertOptionalNotEmpty(bookOptional);
        assertBookAuthorsAndAuthorInitialized(book);
        assertBookAwardsNotInitialized(book);
        assertReviewsNotInitialized(book);
    }

    private void assertBookAuthorsAndAuthorInitialized(Book book) {
        List<BookAuthor> bookAuthors = book.getBookAuthors();
        assertTrue(Hibernate.isInitialized(bookAuthors));
        if (!bookAuthors.isEmpty()) {
            for (BookAuthor bookAuthor : bookAuthors) {
                assertTrue(Hibernate.isInitialized(bookAuthor));
            }
        }
    }

    @Test
    public void givenAuthorAndReviewsDecoratorsApplied_whenFetchingById_thenAuthorsAndReviewsAssociationAreInitialized() {
        BookRetrieveService bookRetrieveService = bookLazilyRetrieveService
                .decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class)
                .decoratedBy(BookAssociatedReviewsRetrieveDecorator.class)
                .get();
        Optional<Book> bookOptional = bookRetrieveService.byId(BOOK_1.getId());
        Book book = (Book) assertOptionalNotEmpty(bookOptional);
        assertBookAuthorsAndAuthorInitialized(book);
        assertBookAwardsNotInitialized(book);
        assertReviewsInitialized(book);
    }

    private void assertReviewsInitialized(Book book) {
        List<Review> reviews = book.getReviews();
        assertTrue(Hibernate.isInitialized(reviews));
        if (!reviews.isEmpty()) {
            for (Review review : reviews) {
                assertTrue(Hibernate.isInitialized(review));
            }
        }
    }

    @Test
    public void givenAuthorAndReviewsDecoratorsApplied_whenFetchingByAuthorId_thenAuthorsAndReviewsAssociationAreInitializedForEachBook() {
        BookRetrieveService bookRetrieveService = bookLazilyRetrieveService
                .decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class)
                .decoratedBy(BookAssociatedReviewsRetrieveDecorator.class)
                .get();
        List<Book> books = bookRetrieveService.byAuthorId(AUTHOR_1.getId());
        assertListIsNotNullAndNotEmpty(books);
        for (Book book : books) {
            assertBookAuthorsAndAuthorInitialized(book);
            assertBookAwardsNotInitialized(book);
            assertReviewsInitialized(book);
        }
    }

    private void assertListIsNotNullAndNotEmpty(List<?> list) {
        assertNotNull(list);
        assertFalse(list.isEmpty());
    }

    @Test
    public void givenNoDecoratorsApplied_whenFetchingByNonExistentAuthorId_thenEmptyListReturned() {
        BookRetrieveService bookRetrieveService = bookLazilyRetrieveService;
        List<Book> books = bookRetrieveService.byAuthorId(NON_EXISTENT_AUTHOR_ID);
        assertListIsNotNullAndEmpty(books);
    }

    private void assertListIsNotNullAndEmpty(List<?> list) {
        assertNotNull(list);
        assertTrue(list.isEmpty());
    }

    @Test
    public void givenAuthorAndReviewsDecoratorsApplied_whenFetchingByPublishingYear_thenAuthorsAndReviewsAssociationAreInitializedForEachBook() {
        BookRetrieveService bookRetrieveService = bookLazilyRetrieveService
                .decoratedBy(BookAssociatedAuthorsRetrieveDecorator.class)
                .decoratedBy(BookAssociatedReviewsRetrieveDecorator.class)
                .get();
        List<Book> books = bookRetrieveService.byPublishingYear(BOOK_3_PUBLISHING_YEAR);
        assertListIsNotNullAndNotEmpty(books);
        for (Book book : books) {
            assertBookAuthorsAndAuthorInitialized(book);
            assertBookAwardsNotInitialized(book);
            assertReviewsInitialized(book);
        }
    }

}
