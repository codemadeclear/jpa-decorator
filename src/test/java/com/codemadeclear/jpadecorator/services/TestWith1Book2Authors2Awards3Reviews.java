package com.codemadeclear.jpadecorator.services;

import com.codemadeclear.jpadecorator.MySpringBootTest;
import com.codemadeclear.jpadecorator.model.entities.Author;
import com.codemadeclear.jpadecorator.model.entities.Award;
import com.codemadeclear.jpadecorator.model.entities.Book;
import com.codemadeclear.jpadecorator.model.entities.Review;
import com.codemadeclear.jpadecorator.repositories.AuthorRepository;
import com.codemadeclear.jpadecorator.repositories.AwardRepository;
import com.codemadeclear.jpadecorator.repositories.BookRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class TestWith1Book2Authors2Awards3Reviews extends MySpringBootTest {

    protected static final String AUTHOR_1_NAME = "John Doe";
    protected static final Author AUTHOR_1 = new Author().setName(AUTHOR_1_NAME);
    protected static final String AUTHOR_2_NAME = "Jane Doe";
    protected static final Author AUTHOR_2 = new Author().setName(AUTHOR_2_NAME);
    protected static final List<Author> AUTHORS = Arrays.asList(AUTHOR_1, AUTHOR_2);
    protected static final String AWARD_1_NAME = "IMPORTANT AWARD";
    protected static final Award AWARD_1 = new Award().setName(AWARD_1_NAME);
    protected static final String AWARD_2_NAME = "NOT SO IMPORTANT AWARD";
    protected static final Award AWARD_2 = new Award().setName(AWARD_2_NAME);
    protected static final List<Award> AWARDS = Arrays.asList(AWARD_1, AWARD_2);
    protected static final int REVIEW_1_RATING = 5;
    protected static final String REVIEW_1_TITLE = "LOVED IT!";
    protected static final String REVIEW_1_CONTENT = "A masterpiece of a book";
    protected static final Review REVIEW_1 = new Review().setRating(REVIEW_1_RATING).setTitle(REVIEW_1_TITLE).setContent(REVIEW_1_CONTENT);
    protected static final int REVIEW_2_RATING = 4;
    protected static final String REVIEW_2_TITLE = "REALLY GOOD!";
    protected static final String REVIEW_2_CONTENT = "A really good book";
    protected static final Review REVIEW_2 = new Review().setRating(REVIEW_2_RATING).setTitle(REVIEW_2_TITLE).setContent(REVIEW_2_CONTENT);
    protected static final int REVIEW_3_RATING = 2;
    protected static final String REVIEW_3_TITLE = "NEH...";
    protected static final String REVIEW_3_CONTENT = "Not a great book";
    protected static final Review REVIEW_3 = new Review().setRating(REVIEW_3_RATING).setTitle(REVIEW_3_TITLE).setContent(REVIEW_3_CONTENT);
    protected static final List<Review> REVIEWS = Arrays.asList(REVIEW_1, REVIEW_2, REVIEW_3);
    protected static final String BOOK_1_TITLE = "Our very first book";
    protected static final int BOOK_1_PUBLISHING_YEAR = 2019;
    protected static final Book BOOK_1 = new Book().setTitle(BOOK_1_TITLE).setPublishingYear(BOOK_1_PUBLISHING_YEAR)
            .addAuthor(AUTHOR_1).addAuthor(AUTHOR_2)
            .addAward(AWARD_1).addAward(AWARD_2)
            .addReview(REVIEW_1).addReview(REVIEW_2).addReview(REVIEW_3);
    protected static final long NON_EXISTENT_AUTHOR_ID = -999L;

    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private AuthorRepository authorRepository;
    @Autowired
    private AwardRepository awardRepository;

    @BeforeEach
    public void setUp() {
        authorRepository.saveAll(AUTHORS);
        awardRepository.saveAll(AWARDS);
        bookRepository.save(BOOK_1);
    }

    @AfterEach
    public void tearDown() {
        BOOK_1.setId(null);
        AUTHORS.forEach(bookAuthor -> bookAuthor.setId(null));
        AWARDS.forEach(award -> award.setId(null));
        REVIEWS.forEach(review -> review.setId(null));
        BOOK_1.getBookAuthors().forEach(bookAuthor -> bookAuthor.setId(null));
        BOOK_1.getBookAwards().forEach(bookAward -> bookAward.setId(null));
    }

}
