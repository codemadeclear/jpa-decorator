package com.codemadeclear.jpadecorator.repositories;

import com.codemadeclear.jpadecorator.model.entities.Award;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AwardRepository extends JpaRepository<Award, Long> {
}
